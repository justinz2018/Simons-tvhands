from my_utils import get_video_ids, get_image
from aps import get_trues
import math
import os
import pickle
import matplotlib.pyplot as plt

x = ['train', 'val', 'test', 'all']
for i in range(len(x)):
    if x[i] == 'validation' or x[i] == 'valid':
        x[i] = 'val'
    x[i] = x[i].lower()

scales = {}
ars = {}  # aspect ratios

for v in x:
    scales[v] = []
    ars[v] = []

for v in x:
    scales_fn = "{}_scales.pkl".format(v)
    ars_fn = "{}_ars.pkl".format(v)
    if os.path.isfile(scales_fn) and os.path.isfile(ars_fn):
        scales[v] = pickle.load(open(scales_fn, 'rb'))
        ars[v] = pickle.load(open(ars_fn, 'rb'))
    else:
        ids = get_video_ids(v)
        for i in ids:
            w = get_image(i)
            t = get_trues(i)
            ws = w.shape[0] * w.shape[1] + 0.0
            for tr in t:
                ts = (tr[1]-tr[0])*(tr[3]-tr[2])
                scale = math.sqrt(ts/ws)
                scales[v].append(scale)
                ar = (tr[1]-tr[0]+0.0)/(tr[3]-tr[2])
                ars[v].append(ar)
        pickle.dump(scales[v], open(scales_fn, 'wb'))
        pickle.dump(ars[v], open(ars_fn, 'wb'))
    print(v)
    plt.hist(scales[v], bins=100)
    plt.xlabel("Scale")
    plt.ylabel("Frequency")
    #plt.title("Scales Histogram for {} Set".format("Training"))#v))
    plt.show()
    plt.hist(ars[v], bins=100)
    #plt.title("Aspect Ratios Histogram for {} Set".format('Training'))#v))
    plt.xlabel("Aspect Ratio")
    plt.ylabel("Frequency")
    plt.show()

