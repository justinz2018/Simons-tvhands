import ast
import cv2
import glob
import random
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
import scipy.io as sio
from visualize import GetPreds, main
from aps import GetBoxes, get_trues, IoU
from aps_ssd import get_boxes_channels

RESULTS_DIR = "/home/jzhang/Downloads/openpose/results"
IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
IDS_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/validIDs.txt"
ITER_NUM = 100000
#CHANNELS = [6, "M", 7, "M", 8, "M", 9, "M", 10, "M"]
CHANNELS = [8, "M"]
TOP_K = 10
NMS_IOU = .9

def get_preds(video_id, ax, img):
	print("VIDEO ID: {}".format(video_id))
	boxes = get_boxes_channels(video_id, "val", ITER_NUM, CHANNELS, nms_iou=NMS_IOU, top_k=TOP_K)
	print("CONFIDENCES")
	for i,bx in enumerate(boxes):
		print(i, bx[4])
		draw_box(img, bx)
		ax.text(bx[1], bx[3], "{}".format(i), bbox={'alpha': .3, 'facecolor': (1, 0, 1)})
	return boxes	

def draw_box(img, bx, color0=(0, 1, 0), color1=(0, 0, 1)):
	l = [(bx[0], bx[2]), (bx[1], bx[2]), (bx[1], bx[3]), (bx[0], bx[3])]
	l = [(round(x[0]), round(x[1])) for x in l]
	cv2.line(img, l[0], l[1], color0, 2)
	cv2.line(img, l[2], l[1], color1, 2)
	cv2.line(img, l[2], l[3], color1, 2)
	cv2.line(img, l[0], l[3], color1, 2)


if __name__ == "__main__":
	GetPreds.get_preds = get_preds
	main()
