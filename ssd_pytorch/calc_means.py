import glob
import cv2
import os


img_path = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
ids = list(open("/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/trainIDs.txt", "r").read().splitlines())
tot_0 = 0
tot_1 = 0
tot_2 = 0
x = 0
for s in ids:
	x += 1
	frame_num = sorted(os.listdir(os.path.join(img_path, s)))[0]
	path = os.path.join(img_path, s, frame_num, "frm_008.png")
	img = cv2.imread(path)
	tot_0 += img[:, :, 0].mean()
	tot_1 += img[:, :, 1].mean()
	tot_2 += img[:, :, 2].mean()
	if x % 100 == 0:
		print("{}/{}".format(x, len(ids)))
tot_0 /= len(ids)
tot_1 /= len(ids)
tot_2 /= len(ids)
print(tot_0, tot_1, tot_2)
