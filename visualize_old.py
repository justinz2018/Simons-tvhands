import ast
import cv2
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
import scipy.io as sio

RESULTS_DIR = "/home/jzhang/project/results_old"
IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
ANNOTATION_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/annos"
if len(sys.argv) > 1:
	s = sys.argv[1]
else:
	s = raw_input("Video id: ")

frame_num = sorted(os.listdir(os.path.join(IMG_DIR, s)))[0]
path = os.path.join(IMG_DIR, s, frame_num, "frm_008.png")
annotation = sio.loadmat(os.path.join(ANNOTATION_DIR, s, frame_num, "frm_008_hand.mat"))

#pr = ast.literal_eval(open(os.path.join(RESULTS_DIR, s)).read())

img = plt.imread(path)

for x in annotation['boxes']:
	l = [(int(x[0][0][0][v][0][0]), int(x[0][0][0][v][0][1])) for v in range(4)]
	cv2.line(img, l[0], l[1], (0, 1, 0), 2)
	cv2.line(img, l[2], l[1], (1, 0, 0), 2)
	cv2.line(img, l[2], l[3], (1, 0, 0), 2)
	cv2.line(img, l[0], l[3], (1, 0, 0), 2)
cmap = matplotlib.cm.get_cmap('Spectral')
_, ax = plt.subplots(1)
ax.set_aspect('equal')
ax.imshow(img)
'''
for x in pr[4]: # right wrist
	ax.add_patch(Circle((x[0], x[1]), 3, color=cmap(x[2])))
for x in pr[7]: # left wrist
	ax.add_patch(Circle((x[0], x[1]), 3, color=cmap(x[2])))
'''
plt.show()

