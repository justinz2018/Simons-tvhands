#!/usr/bin/env python
import os
import sys
module_path = os.path.abspath(os.path.join('ssd_pytorch'))
if module_path not in sys.path:
    sys.path.append(module_path)
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import torch.utils.data as data
import torchvision.transforms as transforms
from torch.utils.serialization import load_lua
from data import v2, v1, AnnotationTransform, VOCDetection, detection_collate, VOCroot, TVHands, BaseTransform
import numpy as np
import cv2 
import glob, ast
from my_utils import get_save_dir
from aps import main, IoU, GetBoxes
from matplotlib import pyplot as plt

torch.backends.cudnn.enabled = False # temporary fix for memory issue

TOP_K = 10
NMS_IOU = .9

IMGS_PATH = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
FN_FORMAT = "{img_id}|{ds_type}|{ITER_NUM}"
USE_ACTUAL_IOU = True

##########
#FORCE_REEVAL = True
FORCE_REEVAL = True

ITER_NUM = 100000

LATE_FUSION = False
DIRECT_FUSION = False 


USE_V3 = False
V3_KERNEL_SZ = 3

AUGMENT = 8 # <= 8 => no augment

#CHANNELS = []
CHANNELS = []
#CHANNELS = [6, "M", 7, "M", 8, "M", 9, "M", 10, "M"]
#CHANNELS = ["N", 6, "M", 7, "M", 8, "M", 9, "M", 10, "M"]
#CHANNELS = ["N",4, "M", 5, "M", 6, "M", 7, "M", 8, "M", 9, "M", 10, "M", 11, "M", 12, "M"]

scale_flow = 1
percent_training = 1
gt_scale = 1
##########

channels_sh, num_channels, no_rgb = get_save_dir(CHANNELS, scale_flow, percent_training, late_fusion=LATE_FUSION, augment=AUGMENT, direct_fusion=DIRECT_FUSION, use_v3=USE_V3, gt_scale=gt_scale, v3_kernel_sz=V3_KERNEL_SZ)
print(num_channels)

RESULTS_FN = FN_FORMAT

if LATE_FUSION:
    from ssd_pytorch.ssd_LF import build_ssd
elif DIRECT_FUSION:
    from ssd_pytorch.ssd_DF import build_ssd
elif USE_V3:
    from ssd_pytorch.ssd_v3 import build_ssd
else:
    from ssd_pytorch.ssd import build_ssd

if len(channels_sh):
    try:
        os.mkdir(os.path.join("ssd_results", channels_sh))
    except:
        pass
    RESULTS_FN = os.path.join(channels_sh, FN_FORMAT)

if no_rgb:
    means = (0,)*(num_channels+3)
else:
    means = (44.31959941978824, 47.74161072727923, 58.569580093625135) + (0,)*(num_channels-3)
size = 300

validation_dataset = TVHands(
    open("/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/validIDs.txt").read().splitlines(),
    IMGS_PATH,
    "/nfs/bigeye/yangwang/DataSets/TVHands/v2/pseudo_annos",
    "/nfs/bigeye/yangwang/DataSets/TVHands/v2/flows",
    BaseTransform(size, means),
    CHANNELS, gt_scale=gt_scale, scale_flow=scale_flow
)
test_dataset = TVHands(
    open("/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/testIDs.txt").read().splitlines(),
    IMGS_PATH,
    "/nfs/bigeye/yangwang/DataSets/TVHands/v2/pseudo_annos",
    "/nfs/bigeye/yangwang/DataSets/TVHands/v2/flows",
    BaseTransform(size, means),
    CHANNELS, gt_scale=gt_scale, scale_flow=scale_flow
)

net = None
first = True

def get_boxes_channels(img_id, ds_type, iter_num=ITER_NUM, channels=CHANNELS, nms_iou=NMS_IOU, top_k=TOP_K, scale_flow=scale_flow, percent_training=percent_training, v3_kernel_sz=V3_KERNEL_SZ):
    global net, RESULTS_FN, num_channels, channels_sh, no_rgb, first
    if channels != CHANNELS:
        RESULTS_FN = FN_FORMAT
        channels_sh, num_channels, no_rgb = get_save_dir(CHANNELS, scale_flow, percent_training)
        if len(channels_sh):
            try:
                os.mkdir(os.path.join("ssd_results", channels_sh))
            except:
                pass
            RESULTS_FN = os.path.join(channels_sh, FN_FORMAT)

        
    #img_id = os.path.basename(path).split("_")[0]
    fn = RESULTS_FN.format(img_id=img_id, ds_type=ds_type, ITER_NUM=iter_num)
    if first:
        print(fn)
        first = False
    if os.path.isfile(os.path.join("ssd_results", fn)) and not FORCE_REEVAL:
        pts = ast.literal_eval(open(os.path.join("ssd_results", fn), "r").read())
        #conf = pts[-1]
        #pts = pts[:8]
        #pts = [min(pts[::2]), max(pts[::2]), min(pts[1::2]), max(pts[1::2]), conf]
    else:
        frame_num = sorted(os.listdir(os.path.join(IMGS_PATH, img_id)))[0]
        path = os.path.join(IMGS_PATH, img_id, frame_num, "frm_008.png")
        image = cv2.imread(path, cv2.IMREAD_COLOR)
        rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        if ds_type == "val":
            x, t = validation_dataset[validation_dataset.rev[(img_id,8)]]
        elif ds_type == "test":
            x, t = test_dataset[test_dataset.rev[(img_id, 8)]]
        else:
            print("Unimplemented ds {}, add data loader to aps_ssd.py".format(ds_type))
            exit(1)

        xx = Variable(x.unsqueeze(0))
        xx = xx.cuda()
        if net is None:
            if LATE_FUSION:
                net = build_ssd('test', size, 2, num_channels, no_rgb, None, None)
            elif DIRECT_FUSION:
                net = build_ssd('test', size, 2, num_channels, no_rgb, None)
            elif USE_V3:
                print("Loading v3")
                net = build_ssd('test', size, 2, num_channels, no_rgb, v3_kernel_sz)
                print("Loaded")
            else:
                net = build_ssd('test', size, 2, num_channels, no_rgb)
            print(channels_sh)
            p = os.path.join('ssd_pytorch/weights/', channels_sh, 'ssd300_0712_iter_{}.pth'.format(ITER_NUM))
            net.load_state_dict(torch.load(p)['net'])
            print("Loaded from {}".format(p))
            net.eval()
            net.cuda()
        y = net(xx)

        detections = y.data

        scale = torch.Tensor([rgb_image.shape[1::-1], rgb_image.shape[1::-1]])

        pts = []

        for j in range(len(detections[0,1,:,0])):
            score = detections[0,1,j,0]
            pt = (detections[0,1,j,1:]*scale).cpu().numpy()
            xmin = min(pt[0], pt[2])
            xmax = max(pt[0], pt[2])
            ymin = min(pt[1], pt[3])
            ymax = max(pt[1], pt[3])
            conf = score
            pts.append((xmin, xmax, ymin, ymax, conf))
        with open(os.path.join("ssd_results", fn), "w") as f:
            f.write(str(pts))
    rem = {}
    top_k_a = []
    for i in range(len(pts)):
        if i in rem: continue
        for j in range(i+1, len(pts)):
            if j in rem: continue
            if IoU(pts[i], pts[j], not USE_ACTUAL_IOU) >= nms_iou:
                rem[j] = 1
        top_k_a.append(pts[i])
        if len(top_k_a) >= top_k:
            break
    return top_k_a

if __name__ == "__main__":

    GetBoxes.get_boxes = get_boxes_channels
    
    if len(sys.argv) > 1:
        get_boxes_channels(sys.argv[1])
    else:
        main("test", True, 0.5, USE_ACTUAL_IOU)
