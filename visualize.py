import ast
import cv2
import glob
import random
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
import scipy.io as sio
from aps import GetBoxes, get_trues, get_trues_pseudo, IoU

RESULTS_DIR = "/home/jzhang/Downloads/openpose/results"
IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
IDS_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/validIDs.txt"

class GetPreds:
        def get_preds(video_id, ax, img):
                path = os.path.join(RESULTS_DIR, video_id+"_keypoints.json")
                cmap = matplotlib.cm.get_cmap('Spectral')
                boxes = []
                pr = ast.literal_eval(open(path, "r").read())
                for p in pr['people']:
                        t = p['hand_right_keypoints']
                        assert(len(t) % 3 == 0)
                        bx = GetBoxes.get_box(t)
                        for x in range(len(t)//3):
                                ax.add_patch(Circle((t[3*x], t[3*x+1]), 3, color=cmap(t[3*x+2])))
                        if len(bx):
                                draw_box(img, bx)
                                ax.text(bx[1], bx[3], "{}".format(len(boxes)), bbox={'alpha': .3, 'facecolor': (1, 0, 1)})
                                boxes.append(bx)

                        t = p['hand_left_keypoints']
                        assert(len(t) % 3 == 0)
                        bx = GetBoxes.get_box(t)
                        for x in range(len(t)//3):
                                ax.add_patch(Circle((t[3*x], t[3*x+1]), 3, color=cmap(t[3*x+2])))
                        if len(bx):
                                draw_box(img, bx)
                                ax.text(bx[1], bx[3], "{}".format(len(boxes)), bbox={'alpha': .5, 'facecolor': (1, 0, 1)})
                                boxes.append(bx)
                return boxes    


def draw_box(img, bx, color0=(0, 1, 0), color1=(0, 0, 1), axis_align=False):
    if not axis_align:
        l = [(bx[2*i], bx[2*i+1]) for i in range(4)]
    else:
        l = [(bx[0], bx[2]), (bx[1], bx[2]), (bx[1], bx[3]), (bx[0], bx[3])]
    l = [(round(x[0]), round(x[1])) for x in l]
    cv2.line(img, l[0], l[1], color0, 2)
    cv2.line(img, l[2], l[1], color1, 2)
    cv2.line(img, l[2], l[3], color1, 2)
    cv2.line(img, l[0], l[3], color1, 2)



def run_once(video_id, show_predictions=True, use_pseudo=True, frame=-1, axis_align=False):
        s = video_id
        frame_num = sorted(os.listdir(os.path.join(IMG_DIR, s)))[0]
        if use_pseudo:
            if frame > 0:
                frm = frame
            else:
                frm = random.randint(8,15)
        else:
            frm = 8
        print(s, frm)
        path = os.path.join(IMG_DIR, s, frame_num, "frm_{:03d}.png".format(frm))

        img = plt.imread(path)
        _, ax = plt.subplots(1)
        ax.set_aspect('equal')

        boxes = []

        if show_predictions:
                boxes += GetPreds.get_preds(s, ax, img)
        num_pred = len(boxes)
        v = get_trues_pseudo(s, frm, axis_align=axis_align)
        for tr in v:
                print(tr)
                draw_box(img, tr, (1, 0, 1), (0, 1, 1), axis_align=axis_align)
        #       ax.text(tr[1], tr[3]+5, "{}".format(len(boxes)), bbox={'alpha': .3, 'facecolor': (0, 1, 1)})
                boxes.append(tr)
        if show_predictions:
                for i in range(num_pred):
                        for j in range(num_pred, len(boxes)):
                                x = IoU(boxes[i], boxes[j])
                                assert(x == IoU(boxes[j], boxes[i]))
                                if x:
                                        print("IoU of {}, {}: {}".format(i, j, x))
                print("-----------")
        ax.imshow(img)
        plt.show()

def main():
        if len(sys.argv) > 1:
                y = sys.argv[1:]
        else:
                y = open(IDS_DIR, "r").read().split()
        random.shuffle(y)
        for i in y:
                run_once(i,False)

if __name__ == "__main__":
        main()
