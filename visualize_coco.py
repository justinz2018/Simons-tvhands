import ast
import cv2
import glob
import random
import os
import pickle
import sys
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
import scipy.io as sio
from aps import GetBoxes, get_trues, get_trues_pseudo, IoU
from get_good_coco import get_pred_boxes, get_gt_wrists, draw, get_pred_wrists, idtopath

IMG_DIR = "/home/jzhang/coco/train2014"
RESULTS_DIR = "/home/jzhang/project/cmu_results_coco"

def imgtoresult(path):
    return os.path.join(RESULTS_DIR, os.path.basename(path)[:-4]+"_keypoints.json")
def imgtoid(path):
    return int(os.path.basename(path)[15:-4])

class GetPreds:
    def get_preds(path, ax, img):
        path = os.path.join(RESULTS_DIR, os.path.basename(path[:-4]+"_keypoints.json"))
        boxes = get_pred_boxes(path)
        i = 0
        if img is not None:
            for bx, _ in boxes:
                if True:#i & 1:
                    draw_box(img, bx, (255, 255, 0), (255, 255, 0))
                else:
                    draw_box(img, bx, (255, 0, 255), (255, 0, 255))
                i += 1
        return boxes    


def draw_box(img, bx, color0=(0, 1, 0), color1=(0, 0, 1)):
        l = [(int(x[0]), int(x[1])) for x in bx]
        print(l)
        cv2.line(img, l[0], l[1], color0, 2)
        cv2.line(img, l[1], l[2], color1, 2)
        cv2.line(img, l[2], l[3], color1, 2)
        cv2.line(img, l[3], l[0], color1, 2)



def run_once(path, show_predictions=True):
        img = plt.imread(path)
        _, ax = plt.subplots(1)
        ax.set_aspect('equal')

        boxes = []
        if show_predictions:
                boxes += GetPreds.get_preds(path, ax, img)
        gt_wrists = get_gt_wrists(int(os.path.basename(path)[15:-4]))
        img = draw(img, gt_wrists)
        ax.imshow(img)
        plt.show()

def dist(a, b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**.5

def get_ratios(path):
    gt_wrists = get_gt_wrists(int(os.path.basename(path)[15:-4]))
    pred_wrists = get_pred_wrists(int(os.path.basename(path)[15:-4]))
    boxes = get_pred_boxes(imgtoresult(path))
    p = []
    for bx,wr in boxes:
        AB = dist(bx[0], bx[1])
        m = 2e15
        for y in gt_wrists:
            m = min(dist(wr,y), m)
        p.append(m/AB)
    return p
    

    

def main():
        if len(sys.argv) > 1:
            if isinstance(sys.argv[1], str) and sys.argv[1][0] == "f":
                y = glob.glob("cmu_results_filtered/*")
                print(y[:10])
                y = [os.path.basename(x) for x in y]
                random.shuffle(y)
            else:
                y = sys.argv[1:]
            y = [idtopath(x) for x in y]
        else:
            y = list(glob.glob(os.path.join(IMG_DIR, "*")))
        random.shuffle(y)
        v = []
        p = 0
        for i in y:
            #v += get_ratios(i)
            #p += 1
            run_once(i)
            #if p % 5000 == 0:
            #    print(p)
        #pickle.dump(v, open("ratios.pkl", "wb"))
        #plt.hist(v, bins=100, range=(0, 4))
        #plt.show()

if __name__ == "__main__":
        main()
