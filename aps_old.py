import ast, glob, multiprocessing, os, sys

from joblib import delayed, Parallel
import numpy as np
import scipy.io as sio
from sklearn.metrics import average_precision_score, precision_recall_curve
import matplotlib.pyplot as plt

def get_trues(video_id):
	IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
	ANNOTATION_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/annos"
	frame_num = sorted(os.listdir(os.path.join(IMG_DIR, video_id)))[0]
	annotation = sio.loadmat(os.path.join(ANNOTATION_DIR, video_id, frame_num, "frm_008_hand.mat"))

	res = []

	for x in annotation['boxes']:
		l = [(int(x[0][0][0][v][0][0]), int(x[0][0][0][v][0][1])) for v in range(4)]
		#### REPLACE HERE AS NECESSARY ###
		res.append(((l[0][0]+l[1][0])/2, (l[0][1]+l[1][1])/2))

		#########
	return res

def calc_hits(trues, preds, tr):
	preds.sort(key=lambda x: 1-x[2])
	taken = {}
	res = [] # [(y_true1, y_pred1), (y_true2, y_pred2), ...]
	for pred in preds:
		m_dist = 2e15
		pr = None
		for true in trues:
			if true in taken: continue
			d = (true[1]-pred[1])**2 + (true[0]-pred[0])**2
			if d < min(tr*tr, m_dist):
				m_dist = d
				pr = true
		if pr is None:
			res.append((0, pred[2]))
		else:
			res.append((1, pred[2]))
			taken[pr] = 1
	num_missed = 0
	for true in trues:
		if true not in taken:
			num_missed += 1
	return (res, num_missed)

def calc_all_hits(threshold):
	tot_res = []
	tot_num_missed = 0
	tot_hands = 0
	for x in glob.glob(os.path.join("results_old", "*")):
		x = os.path.basename(x)
		y = ast.literal_eval(open(os.path.join("results_old", x), "r").read())

		preds = y[4]+y[7]
		trues = get_trues(x)
		tot_hands += len(trues)

		res, num_missed = calc_hits(trues, preds, threshold)
		tot_res += res
		tot_num_missed += num_missed
	
	tot_res.sort(key=lambda x:-x[1])
	y_true = [a[0] for a in tot_res]
	y_scores = [a[1] for a in tot_res]

	return {
		"y_true": y_true,
		"y_scores": y_scores,
		"tot_num_missed": tot_num_missed,
		"tot_hands": tot_hands
	}
	
def auc(x_coords, y_coords):
	area = 0
	for i in range(len(x_coords)-1):
		dx = x_coords[i+1]-x_coords[i]
		dy = (y_coords[i+1]+y_coords[i])/2.0
		area += dx*dy
	return abs(area)

def get_precision_recall(y_true, y_scores, num_hands):
	precision = []
	recall = []
	tp = 0
	for i in range(len(y_true)):
		if y_true[i]:
			tp += 1
		precision.append((tp+0.0)/(i+1))
		recall.append((tp+0.0)/num_hands)
	'''	
	# let's draw a straight line to (1, 0)
	slope = precision[-1]/(recall[-1]-1.0)
	#y-y1 = m(x-x1)
	i = recall[-1]+.01
	while i < 1.0001:
		recall.append(i)
		precision.append(slope*(i-1))
		i += .01
	'''
	return (precision, recall)

if __name__ == "__main__":
	r = calc_all_hits(100000)
	y_true, y_scores, num_missed, num_hands =\
		(r["y_true"], r["y_scores"], r["tot_num_missed"], r["tot_hands"])
	precision, recall = get_precision_recall(y_true, y_scores, num_hands)
	
	plt.plot(recall, precision,
		label="Precision/Recall, AP={}".format(auc(recall, precision)))
	plt.legend(loc="lower left")
	plt.xlim([0, 1.05])
	plt.ylim([0, 1.05])
	plt.xlabel("Recall")
	plt.ylabel("Precision")
	plt.title("Precision vs. Recall, default threshold")
	plt.show()

	def get_ap(threshold):
		r = calc_all_hits(threshold)
		y_true, y_scores, num_missed, num_hands =\
			(r["y_true"], r["y_scores"], r["tot_num_missed"], r["tot_hands"])
		precision, recall = get_precision_recall(y_true, y_scores, num_hands)
		return (auc(recall, precision), threshold)
		

	thresholds = list(range(1, 101))
	aps = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=5)(
		delayed(get_ap)(i) for i in thresholds
	)
	assert(aps == sorted(aps, key=lambda x:x[1]))
	aps = [a[0] for a in aps]
	plt.plot(thresholds, aps, label="Threshold/AP")
	plt.legend(loc="lower left")
	plt.ylim([0, 1.05])
	plt.xlabel("Threshold")
	plt.ylabel("AP")
	plt.show()	
