import os
import cv2
import glob

RESULTS_DIR = "/home/jzhang/Downloads/openpose/results"
IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
SPLITS_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits"

def get_path(video_id, frame_num=8):
	s = video_id
	sh = sorted(os.listdir(os.path.join(IMG_DIR, s)))[0]
	return os.path.join(IMG_DIR, s, sh, "frm_{:03}.png".format(frame_num))

def get_image(video_id, frame_num=8):
	x = get_path(video_id, frame_num)
	return cv2.imread(x)

def get_video_ids(ds="all"):
    if ds.lower() == "all":
        return open(os.path.join(SPLITS_DIR, "allIDs.txt")).read().splitlines()
    elif ds.lower() == "train":
        return open(os.path.join(SPLITS_DIR, "trainIDs.txt")).read().splitlines()
    elif ds.lower() in ["val", "validation", "valid"]:
        return open(os.path.join(SPLITS_DIR, "validIDs.txt")).read().splitlines()
    elif ds.lower() == "test":
        return open(os.path.join(SPLITS_DIR, "testIDs.txt")).read().splitlines()
    else:
        raise Exception("No ds found for {}".format(ds))

def get_save_dir(channels, scale_flow, percent_training, late_fusion, direct_fusion, augment=8, use_v3=False, gt_scale=1,v3_kernel_sz=3):
    channels = channels[:]
    no_rgb = False
    for w in channels:
        if isinstance(w, str) and w[0] == 'N':
            no_rgb = True
            break
    channels = [x for x in channels if not (isinstance(x, str) and x[0] == 'N')]
    if no_rgb:
        channels = ['N',]+channels

    num_channels = sum([1 if (isinstance(x, str) and x[0] == 'M') else 2 if isinstance(x, int) else 0 for x in channels])
    if not no_rgb:
        num_channels += 3
    channels_sh = "".join([x[0] if isinstance(x, str) else str(x) for x in channels])
    if late_fusion:
        channels_sh += "_LF"
    if augment > 8:
        channels_sh += "_AUG{}".format(augment)
    if use_v3:
        channels_sh += "_v3"
        channels_sh += "x{}".format(v3_kernel_sz)
    if direct_fusion:
        channels_sh += "_DF"
    assert gt_scale >= 1, "Ground truth box scale must be >= 1"
    if gt_scale != 1:
        channels_sh += "_GTSCALE{}".format(gt_scale)


    if scale_flow != 1:
            channels_sh = os.path.join(channels_sh, "scale{}".format(scale_flow))
    if percent_training != 1:
            channels_sh = os.path.join(channels_sh, "percent_training{}".format(percent_training))

    return (channels_sh, num_channels, no_rgb)


