LIMIT=1; % only 1 segment per video

path = '/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips/';
tmp=dir(path);
disp(tmp);
videos=setdiff({tmp.name},{'.','..'});
m = parcluster('local');
m.NumWorkers = 40;
saveProfile(m);
addpath(genpath('utils'));
x = fopen('errors4.txt');
t = fgetl(x);
c = containers.Map;
while ischar(t)
    v = strsplit(t, '|');
    c(v{1}) = 1;
    t = fgetl(x);
end
videos = keys(c);
for i=1:numel(videos)
    video=videos{i};

    tmp=dir(sprintf(strcat(path, '%s/'),video));
    shots=setdiff({tmp.name},{'.','..'});
    delete(sprintf('%s/%s/*.png', video, shots{1}));

    for j=1:min(LIMIT, numel(shots))
        shot=shots{j};
        p = sprintf(strcat(path, '%s/%s/'), video, shot);
        q = sprintf('%s/%s/', video, shot);
        system(['mkdir -p', q]);
        try
            dir_get_epicflow(p, q, 40);
        catch
            f = fopen('errors.txt', 'a');
            fprintf(f, '%s\n', video);
            fclose(f);
        end
	
    end

    for i=1:15
        u_p = sprintf('%s/%s/frm_%03d_u.txt', video, shots{1}, i);
        v_p = sprintf('%s/%s/frm_%03d_v.txt', video, shots{1}, i);
        try
            img = readFlowFile(sprintf('%s/%s/frm_%03d.flo', video, shots{1}, i));
            dlmwrite(u_p, img(:, :, 1), 'delimiter', ' ');
            dlmwrite(v_p, img(:, :, 2), 'delimiter', ' ');
        catch
            f = fopen('errors2.txt', 'a');
            fprintf(f, '%s/%d\n', video, i);
            fclose(f);
        end
    end

end
