#!/usr/bin/env python
import ast, glob, multiprocessing, os, sys, random

from joblib import delayed, Parallel
import numpy as np
import scipy.io as sio
from sklearn.metrics import average_precision_score, precision_recall_curve
import matplotlib.pyplot as plt
from shapely.geometry import Polygon

USE_ACTUAL_IOU = False

def get_trues(video_id, axis_align=True):
        return get_trues_pseudo(video_id, 8, axis_align)
def get_trues_pseudo(video_id, frame, axis_align=True):
        IMG_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/clips"
        ANNOTATION_DIR = "/nfs/bigeye/yangwang/DataSets/TVHands/v2/pseudo_annos"
        res = []
        with open(os.path.join(ANNOTATION_DIR, "{}|{}".format(video_id, frame))) as f:
            for l in f.read().splitlines():
                p = tuple(map(lambda x: int(float(x)), l.split()))
                if axis_align:
                    xs = [p[2*i] for i in range(4)]
                    ys = [p[2*i+1] for i in range(4)]
                    res.append((min(xs), max(xs), min(ys), max(ys)))
                else:
                    res.append(p)
        return res

def sign(a, b, c):
    x = (b[0]-a[0], b[1]-a[1])
    y = (c[0]-b[0], c[1]-b[1])
    return x[0]*y[1] > y[0]*x[1]

    
def orient(p):
        v = sign(p[0], p[1], p[2])
        if not v:
            # 0,1,2,3 | 0,1,3,2 | 0,2,3,1
            v2 = sign(p[0], p[2], p[3])
            if not v2:
                # 0,1,2,3
                pass
            else:
                # 0,1,3,2 | 0,2,3,1
                v3 = sign(p[0], p[1], p[3])
                if not v3:
                    # 0,1,3,2
                    p[2], p[3] = p[3], p[2]
                else:
                    # 0,2,3,1
                    p[1], p[3] = p[3], p[1]
                    p[2], p[3] = p[3], p[2]
        else:
            # 0,3,1,2 | 0,3,2,1 | 0,2,1,3
            v2 = sign(p[0], p[1], p[3])
            if not v2:
                # 0,2,1,3
                p[1], p[2] = p[2], p[1]
            else:
                # 0,3,1,2 | 0,3,2,1
                v3 = sign(p[0], p[2], p[3])
                if not v3:
                    # 0,3,1,2
                    p[1], p[2] = p[2], p[1]
                    p[2], p[3] = p[3], p[2]
                else:
                    # 0,3,2,1
                    p[1], p[3] = p[3], p[1]
        return p


def IoU(b1, b2, axis_align=True):
    n1 = b1[:]
    n2 = b2[:]
    if 8 <= len(n2) <= 9 and 8 <= len(n1) <= 9:
        assert not USE_ACTUAL_IOU
        axis_align = False
    if not axis_align:
        assert USE_ACTUAL_IOU
        if len(n1) == 5: n1 = n1[:4]
        if len(n1) == 4:
            n1 = [n1[0], n1[2], n1[0], n1[3], n1[1], n1[2], n1[1], n1[3], -1]
        if len(n2) == 5: n2 = n2[:4]
        if len(n2) == 4:
            n2 = [n2[0], n2[2], n2[0], n2[3], n2[1], n2[2], n2[1], n2[3], -1]
    else:
        assert not USE_ACTUAL_IOU
        if len(n1) == 9: n1 = n1[:8]
        if len(n2) == 9: n2 = n2[:8]
        if len(n1) == 8:
            n1 = [min(n1[::2]), max(n1[::2]), min(n1[1::2]), max(n1[1::2])]
        if len(n2) == 8:
            n2 = [min(n2[::2]), max(n2[::2]), min(n2[1::2]), max(n2[1::2])]
    if axis_align:
        assert not USE_ACTUAL_IOU
        assert len(n1) <= 5 and  len(n2) <= 5
        x_vals = sorted([(n1[0], 0), (n1[1], 0), (n2[0], 1), (n2[1], 1)])
        y_vals = sorted([(n1[2], 0), (n1[3], 0), (n2[2], 1), (n2[3], 1)])
        if x_vals[1][1] == x_vals[0][1]:
                return 0
        if y_vals[1][1] == y_vals[0][1]:
                return 0
        dx = abs(x_vals[2][0]-x_vals[1][0])
        dy = abs(y_vals[2][0]-y_vals[1][0])
        intersect = dx*dy
        t1 = abs(x_vals[0][0]-x_vals[2][0])*abs(y_vals[0][0]-y_vals[2][0])
        t2 = abs(x_vals[1][0]-x_vals[3][0])*abs(y_vals[1][0]-y_vals[3][0])
        return intersect / (t1 + t2 - intersect)
    else:
        assert USE_ACTUAL_IOU
        try:
            n1 = [(n1[2*x], n1[2*x+1]) for x in range(4)]
            n2 = [(n2[2*x], n2[2*x+1]) for x in range(4)]
            n1 = orient(n1)
            n2 = orient(n2)
            p1 = Polygon(n1).buffer(0)
            p2 = Polygon(n2).buffer(0)
            i = p1.intersection(p2).area
            u = p1.union(p2).area
            if u < .001:
                assert False
                return -1
            return i / u
        except Exception as e:
            print(str(e))
            print("ERR ON IoU")
            return -1
        
def calc_hits(trues, preds, tr):
        preds.sort(key=lambda x: -x[4])
        taken = {}
        res = [] # [(y_true1, y_pred1), (y_true2, y_pred2), ...]
        for pred in preds:
                m_dist = -2e15
                pr = None
                for (i, true) in enumerate(trues):
                        if i in taken: continue
                        d = IoU(pred, true, not USE_ACTUAL_IOU)
                        if d > max(tr, m_dist):
                                m_dist = d
                                pr = i
                if pr is None:
                        res.append((0, pred[4]))
                else:
                        res.append((1, pred[4]))
                        taken[pr] = 1
        num_missed = 0
        for true in trues:
                if true not in taken:
                        num_missed += 1
        return (res, num_missed)

class GetBoxes:
        def get_box(t):
            xmin = xmax = t[0]
            ymin = ymax = t[1]
            conf = 0.0
            for (i, z) in enumerate(t):
                    if i % 3 == 0:
                            xmin = min(xmin, z)
                            xmax = max(xmax, z)
                    elif i % 3 == 1:
                            ymin = min(ymin, z)
                            ymax = max(ymax, z)
                    else:
                            conf += z
            conf /= len(t)
            if xmin == xmax == ymin == ymax == 0:
                    return ()
            return (xmin, xmax, ymin, ymax, conf)

        def get_boxes(img_id, axis_align=True):
                if not axis_align:
                    # return [x[0] for x in get_boxes_within(path)]
                    # TODO: read from file
                    raise Exception("unimplemented")
                path = "cmu_results_test/{}_keypoints.json".format(img_id)
                y = ast.literal_eval(open(path, "r").read())
                preds = []
                for p in y['people']:
                        t = p['hand_left_keypoints']
                        t2 = p['hand_right_keypoints']
                        a = GetBoxes.get_box(t)
                        a2 = GetBoxes.get_box(t2)
                        if len(a):
                                preds.append(a)
                        if len(a2):
                                preds.append(a2)
                return preds

        
def calc_all_hits(threshold, ds_type):
        tot_res = []
        tot_num_missed = 0
        tot_hands = 0
        tmp = list(open("/nfs/bigeye/yangwang/DataSets/TVHands/v2/splits/testIDs.txt").read().splitlines())
        #tmp = list(glob.glob(os.path.join("cmu_results_{}".format(ds_type), "*")))
        random.shuffle(tmp)
        i = 0
        for x in tmp:
                i += 1
                #img_id = os.path.basename(x).split("_")[0]

                preds = GetBoxes.get_boxes(x, ds_type)

                trues = get_trues(x, not USE_ACTUAL_IOU)
                tot_hands += len(trues)

                res, num_missed = calc_hits(trues, preds, threshold)
                tot_res += res
                tot_num_missed += num_missed
        
        tot_res.sort(key=lambda x:-x[1])
        y_true = [a[0] for a in tot_res]
        y_scores = [a[1] for a in tot_res]

        return {
                "y_true": y_true,
                "y_scores": y_scores,
                "tot_num_missed": tot_num_missed,
                "tot_hands": tot_hands
        }
        
def auc(x_coords, y_coords):
        area = 0
        for i in range(len(x_coords)-1):
                dx = x_coords[i+1]-x_coords[i]
                dy = (y_coords[i+1]+y_coords[i])/2.0
                area += dx*dy
        return abs(area)

def get_precision_recall(y_true, y_scores, num_hands):
        precision = []
        recall = []
        tp = 0
        for i in range(len(y_true)):
                if y_true[i]:
                        tp += 1
                precision.append((tp+0.0)/(i+1))
                recall.append((tp+0.0)/num_hands)
        '''     
        # let's draw a straight line to (1, 0)
        slope = precision[-1]/(recall[-1]-1.0)
        #y-y1 = m(x-x1)
        i = recall[-1]+.01
        while i < 1.0001:
                recall.append(i)
                precision.append(slope*(i-1))
                i += .01
        '''
        return (precision, recall)

def get_ap(threshold, ds_type):
        r = calc_all_hits(threshold, ds_type)
        y_true, y_scores, num_missed, num_hands =\
                (r["y_true"], r["y_scores"], r["tot_num_missed"], r["tot_hands"])
        precision, recall = get_precision_recall(y_true, y_scores, num_hands)
        return (auc(recall, precision), threshold)

first = True

from my_utils import get_save_dir
def get_get_boxes_channels(ds_type, iter_num, channels, late_fusion, augment, direct_fusion, use_v3, nms_iou=0.9, top_k=10, scale_flow=1, percent_training=1, gt_scale=1,v3_kernel_sz=3,force_path=None):
    def m_get_boxes_channels(img_id, _):
        global first
        FN_FORMAT = "{img_id}|{ds_type}|{ITER_NUM}"
        RESULTS_FN = FN_FORMAT
        if force_path:
            channels_sh = force_path
        else:
            channels_sh, num_channels, no_rgb = get_save_dir(channels, scale_flow, percent_training, late_fusion=late_fusion, augment=augment, direct_fusion=direct_fusion, use_v3=use_v3, gt_scale=gt_scale,v3_kernel_sz=v3_kernel_sz)
        if first:
            first = False
            if force_path: print(force_path)
            else: print(channels_sh)
        RESULTS_FN = os.path.join(channels_sh, FN_FORMAT)
            
        #img_id = os.path.basename(path).split("_")[0]
        fn = RESULTS_FN.format(img_id=img_id, ds_type=ds_type, ITER_NUM=iter_num)
        pts = ast.literal_eval(open(os.path.join("ssd_results", fn), "r").read())

        rem = {}
        top_k_a = []
        for i in range(len(pts)):
            if i in rem: continue
            for j in range(i+1, len(pts)):
                if j in rem: continue
                if IoU(pts[i], pts[j], not USE_ACTUAL_IOU) >= nms_iou:
                    rem[j] = 1
            top_k_a.append(pts[i])
            if len(top_k_a) >= top_k:
                break
        return top_k_a
    return m_get_boxes_channels

#def get_get_boxes_channels(ds_type, iter_num, channels, late_fusion, augment, direct_fusion, use_v3, nms_iou=0.9, top_k=10, scale_flow=1, percent_training=1, gt_scale=1,v3_kernel_sz=3,force_path=None):
TO_PLOT = [ 
           #("CMUPOSE", "Multi-Person Pose Estimation"),
#            ([100000, [], False, 8, False, False], "Vanilla SSD"),
#           ([160000, [], False, 8, False, False], "16k Vanilla SSD"),

#            ([100000, [], False, 8, False, True], "SSD with adjustments (7x7 kernel)"),
#            ([100000, [8, 'M'], False, 8, False, False], "Early Fusion RGB+8M"),

            ([50000, [8, 'M'], True, 8, False, False], "Late Fusion RGB+8M"),
            ([50000, [6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], True, 8, False, False], "Late Fusion RGB+6M-10M"),
            ([100000, [6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], False, 8, False, False], "Early Fusion RGB+6M-10M"),
            ([50000, [6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], False, 8, True, False], "Direct Fusion RGB+6M-10M"),
            ([50000, [6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], True, 15, False, False], "Late Fusion RGB+6M-10M+Aug"),
#
#            ([100000, ["N", 6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], False, 8, False, False], "Flow-Only 6M-10M"),
#            ([50000, [6, "M", 7, 'M', 8, 'M', 9, 'M', 10, 'M'], False, 15, False, False], "6M-10M with flow augment"),
#            ([100000, [6, 'M', 7, 'M', 8, 'M', 9, 'M', 10, 'M'], True, 8, False, False], "LF6M-10M 7x7"),
            #([100000, [], False, 8, False, True, .9, 10, 1, 1, 1, 3], "SSD Adjusted Scale/Ratio 3x3"),
            #([100000, [], False, 8, False, True, .9, 10, 1, 1, 1, 5], "SSD Adjusted Scale/Ratio 5x5"),
            #([100000, [], False, 8, False, True, .9, 10, 1, 1, 1, 7], "SSD Adjusted Scale/Ratio 7x7"),
            ]

import pickle

def main(ds_type, from_aps_ssd=False, IOU_THRESH=0.5, actual_iou=USE_ACTUAL_IOU):
        global USE_ACTUAL_IOU
        USE_ACTUAL_IOU = actual_iou
        old_get_boxes = GetBoxes.get_boxes
        global first
        plot = []
        print("Acutal IoU", USE_ACTUAL_IOU)
        if True:
            if from_aps_ssd:
                first = False
                r = calc_all_hits(IOU_THRESH, ds_type)
                y_true, y_scores, num_missed, num_hands =\
                        (r["y_true"], r["y_scores"], r["tot_num_missed"], r["tot_hands"])
                p, r = get_precision_recall(y_true, y_scores, num_hands)
                n = ""
                plot.append((p, r, n))
                pickle.dump((p,r), open("PR.p", "wb"))
            else:
                for p, n in TO_PLOT:
                    first = True
                    if isinstance(p, str) and p.lower() == "cmupose":
                        assert len(plot) == 0
                        GetBoxes.get_boxes = old_get_boxes
                    else:
                        GetBoxes.get_boxes = get_get_boxes_channels(ds_type, *p)
                    r = calc_all_hits(IOU_THRESH, ds_type)
                    y_true, y_scores, num_missed, num_hands =\
                            (r["y_true"], r["y_scores"], r["tot_num_missed"], r["tot_hands"])
                    p, r = get_precision_recall(y_true, y_scores, num_hands)
                    plot.append((p, r, n))
            
            for precision, recall, name in plot:
                k = auc(recall, precision)
                print(name, k)
                plt.plot(recall, precision, label=" ".join([name,"AP={}".format(round(k, 2))]))

            plt.legend(loc="upper right", prop={'size': 10})
            plt.xlim([0, 1.05])
            plt.ylim([0, 1.05])
            plt.xlabel("Recall")
            plt.ylabel("Precision")
            plt.show()
            #plt.savefig("AP.png")

        thresholds = [.01*i for i in range(0, 101)]
        if from_aps_ssd:
            first = False
            aps = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=5)(
                    delayed(get_ap)(i, ds_type) for i in thresholds
            )
            aps = [a[0] for a in aps]
            plt.plot(thresholds, aps, label="AUC={}".format(auc(thresholds, aps)))
            pickle.dump((thresholds,aps), open("01.p", "wb"))
        else:
            first = True
            for p, n in TO_PLOT:
                if isinstance(p, str) and p.lower() == "cmupose":
                    assert first
                    GetBoxes.get_boxes = old_get_boxes
                else:
                    GetBoxes.get_boxes =  get_get_boxes_channels(ds_type, *p)
                first = False
                aps = Parallel(n_jobs=multiprocessing.cpu_count(), verbose=5)(
                        delayed(get_ap)(i, ds_type) for i in thresholds
                )
                aps = [a[0] for a in aps]
                k = auc(thresholds, aps)
                print(n, k)
                plt.plot(thresholds, aps, label=n+" AUC={}".format(round(k, 2)))
        plt.legend(loc="upper right", prop={'size': 10})
        plt.xlim([0, 1.05])
        plt.ylim([0, 1.05])
        plt.xlabel("Threshold")
        plt.ylabel("AP")
        plt.show()
        #plt.savefig("01.png")

if __name__ == "__main__":
        main("test")
